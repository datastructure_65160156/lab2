public class ArrayDeletionsLab {

    public static void main(String[] args) {
        int[] arr = {1, 2, 3, 4, 5};

        // Example usage of deleteElementByIndex
        int indexToDelete = 2;
        int[] updatedArr1 = deleteElementByIndex(arr, indexToDelete);
        printArray(updatedArr1);

        // Example usage of deleteElementByValue
        int valueToDelete = 4;
        int[] updatedArr2 = deleteElementByValue(arr, valueToDelete);
        printArray(updatedArr2);
    }

    public static int[] deleteElementByIndex(int[] arr, int index) {
        if (index < 0 || index >= arr.length) {
            // Index is out of bounds, return the original array
            return arr;
        }

        int[] updatedArr = new int[arr.length - 1];
        int j = 0; // Pointer for the updated array

        for (int i = 0; i < arr.length; i++) {
            if (i != index) {
                updatedArr[j++] = arr[i];
            }
        }

        return updatedArr;
    }

    public static int[] deleteElementByValue(int[] arr, int value) {
        int indexToDelete = -1;

        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == value) {
                indexToDelete = i;
                break;
            }
        }

        if (indexToDelete == -1) {
            // Value not found, return the original array
            return arr;
        }

        int[] updatedArr = new int[arr.length - 1];
        int j = 0; // Pointer for the updated array

        for (int i = 0; i < arr.length; i++) {
            if (i != indexToDelete) {
                updatedArr[j++] = arr[i];
            }
        }

        return updatedArr;
    }

    public static void printArray(int[] arr) {
        for (int num : arr) {
            System.out.print(num + " ");
        }
        System.out.println();
    }
}
