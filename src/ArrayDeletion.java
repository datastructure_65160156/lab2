public class ArrayDeletion {
    public static int[] deleteElementByIndex(int[] arr, int index) {
        int[] newArray = new int[arr.length - 1];
        for (int i = 0; i < index; i++) {
            newArray[i] = arr[i];
        }
        for (int i = index; i < arr.length - 1; i++) {
            newArray[i] = arr[i + 1];
        }
        return newArray;
    }

    public static int[] deleteElementByValue(int[] arr, int value) {
        int[] newArray = new int[arr.length - 1];
        for (int i = 0, j = 0; i < arr.length; i++) {
            if (arr[i] != value) {
                newArray[j] = arr[i];
                j++;
            }
        }
        return newArray;
    }

    public static void main(String[] args) {
        int[] arr = {1, 2, 3, 4, 5};
        int index = 2;
        int value = 4;
        System.out.println("First Original Array");
        for (int m : arr) System.out.print(m + " ");
        System.out.println();
        System.out.println("Array after deleting element at index 2");
        arr = deleteElementByIndex(arr, index);
        for (int m : arr) System.out.print(m + " ");
        System.out.println();
        System.out.println("Array after deleting element with value 4");
        arr = deleteElementByValue(arr, value);
        for (int m : arr) System.out.print(m + " ");
        int[] arr2 = {4, 2, 5, 6 ,7};
        index = 3;
        value = 2;
        System.out.println();
        System.out.println("Second Original Array");
        for (int m : arr2) System.out.print(m + " ");
        System.out.println();
        System.out.println("Array after deleting element at index 3");
        arr2 = deleteElementByIndex(arr2, index);
        for (int m : arr2) System.out.print(m + " ");
        System.out.println();
        System.out.println("Array after deleting element with value 2");
        arr2 = deleteElementByValue(arr2, value);
        for (int m : arr2) System.out.print(m + " ");
    }

}
