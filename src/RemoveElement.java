public class RemoveElement {
    public static void main(String[] args) {
        int[] nums = {3, 2, 2, 3};
        int val = 3;
        System.out.println("Original Array");
        for (int num : nums) System.out.print(num + " ");
        System.out.println();
        System.out.println("Array after removing element with value 3");
        int len = removeElement(nums, val);
        System.out.println("K = " + len);
        nums = returnNums(nums, val, len);
        for (int i = 0; i < len; i++) System.out.print(nums[i] + " ");
        System.out.println();
        int[] nums2 = {0, 1, 2, 2, 3, 0, 4, 2};
        val = 2;
        System.out.println("Original Array");
        for (int num : nums2) System.out.print(num + " ");
        System.out.println();
        System.out.println("Array after removing element with value 2");
        len = removeElement(nums2, val);
        System.out.println("K = " + len);
        nums2 = returnNums(nums2, val, len);
        for (int i = 0; i < len; i++) System.out.print(nums2[i] + " ");
    }

    private static int[] returnNums(int[] nums, int val, int len) {
        int index = 0;
        int k =0;
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] != val) {
                index++;
            }
        }
        int[] newArray = new int[index];
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] != val) {
                newArray[k] = nums[i];
                k++;
            }
        }
        return newArray;
    }

    private static int removeElement(int[] nums, int val) {
        int index = 0;
        int k =0;
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] != val) {
                index++;
            }
        }
        int[] newArray = new int[index];
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] != val) {
                newArray[k] = nums[i];
                k++;
            }
        }
        return k;
    }

}
